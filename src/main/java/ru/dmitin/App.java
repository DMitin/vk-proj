package ru.dmitin;

import com.vk.api.sdk.client.VkApiClient;
import com.vk.api.sdk.client.actors.UserActor;
import com.vk.api.sdk.exceptions.ApiException;
import com.vk.api.sdk.exceptions.ClientException;
import com.vk.api.sdk.httpclient.HttpTransportClient;
import com.vk.api.sdk.objects.groups.responses.GetBannedResponse;
import com.vk.api.sdk.objects.users.UserXtrCounters;
import com.vk.api.sdk.queries.groups.GroupsGetBannedQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;

public class App 
{
    private static final Logger LOG = LoggerFactory.getLogger(App.class);

    public static void main( String[] args ) throws ClientException, ApiException {
        Properties properties = loadConfiguration();
        LOG.error("Hello!");

        String user = properties.getProperty("user.id");
        String token = properties.getProperty("token");

        VkApiClient vk = new VkApiClient(new HttpTransportClient());
        UserActor actor = new UserActor(Integer.parseInt(user), token);
        //List<UserXtrCounters> getUsersResponse = vk.users().get(actor).userIds(user).execute();
        //GetBannedResponse getBannedResponse = vk.groups().getBanned(actor, 53594341).execute();



    }


    private static Properties loadConfiguration() {
        Properties properties = new Properties();
        try (InputStream is = App.class.getResourceAsStream("/config.properties")) {
            properties.load(is);
        } catch (IOException e) {
            LOG.error("Can't load properties file", e);
            throw new IllegalStateException(e);
        }

        return properties;
    }
}
